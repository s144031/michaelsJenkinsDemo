import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;

import michaelsjenkinsdemo.MichaelsJenkinsDemo;

public class StepdefinitionsTest {

    MichaelsJenkinsDemo demo = new MichaelsJenkinsDemo();
    String result;

    @When("user runs the program")
    public void user_runs_the_program() {
        // Write code here that turns the phrase above into concrete actions
        this.result = demo.getMessage();
    }

    @Then("result is {string}")
    public void result_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals("Hello from Michaels Jenkins Demo!", this.result);
    }

}
